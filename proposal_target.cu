
/*!
 * \file proposal_target.cu
 * \brief proposal_target Operator
*/
#include <dmlc/logging.h>
#include <dmlc/parameter.h>
#include <mxnet/operator.h>
#include <mshadow/tensor.h>
#include <mshadow/cuda/reduce.cuh>
#include <thrust/sort.h>
#include <thrust/execution_policy.h>
#include <thrust/functional.h>

#include <map>
#include <vector>
#include <string>
#include <utility>
#include <ctime>
#include <iostream>
#include <algorithm>

#include "./operator_common.h"
#include "./mshadow_op.h"
#include "proposal_target-inl.h"

#define DIVUP(m, n) ((m) / (n) + ((m) % (n) > 0))

#define FRCNN_CUDA_CHECK(condition) \
  /* Code block avoids redefinition of cudaError_t error */ \
  do { \
    cudaError_t error = condition; \
    CHECK_EQ(error, cudaSuccess) << " " << cudaGetErrorString(error); \
} while (0)

namespace mshadow {
namespace cuda {

    template<typename Dtype>
    __global__ void Add_gt_to_rois( const int count,
                                    const Dtype* gt_box,
                                    Dtype* added_rois) {
      for (int index = blockIdx.x * blockDim.x + threadIdx.x;
           index < count;
           index += blockDim.x * gridDim.x) {

        added_rois[index * 5] = 0;
        added_rois[index * 5 + 1] = gt_box[index * 5];
        added_rois[index * 5 + 2] = gt_box[index * 5 + 1];
        added_rois[index * 5 + 3] = gt_box[index * 5 + 2];
        added_rois[index * 5 + 4] = gt_box[index * 5 + 3];


      }
    }
    //overlaps shape:n*k ,element:[n_gtbox_element]
    //rois is [batch_idx,x0,y0,x1,y1]
    //gt_box is [x0,y0,x1,y1,cls]
  template<typename Dtype>
  __global__ void Bbox_overlaps( const int count,
          const int gt_box_count,
                                    const Dtype* query_boxes,
                                    const Dtype* boxes,
                                    float* overlaps) {
    for (int index = blockIdx.x * blockDim.x + threadIdx.x;
         index < count;
         index += blockDim.x * gridDim.x) {
      int idx_gt_box = index % gt_box_count;
      int idx_rois = index / gt_box_count;
      int n = idx_rois * 5 + 1;
      int k = idx_gt_box * 5;
        Dtype box_area=0;
        Dtype all_area=0;
      Dtype query_box_area =
              (query_boxes[k + 2] - query_boxes[k] + 1) * (query_boxes[k + 3] - query_boxes[k + 1] + 1);
      Dtype iw = min(boxes[n + 2], query_boxes[k + 2]) - max(boxes[n], query_boxes[k]) + 1;
      Dtype ih = 0;
      if (iw > 0) {
        ih = min(boxes[n+3], query_boxes[k+3]) - max(boxes[n+1], query_boxes[k+1]) + 1;
        if (ih > 0) {
              box_area = (boxes[n + 2] - boxes[n] + 1) * (boxes[n + 3] - boxes[n + 1] + 1);
              all_area = float(box_area + query_box_area - iw * ih);
              overlaps[idx_gt_box + (idx_rois*gt_box_count)] = iw * ih / all_area;
            }else {

            overlaps[idx_gt_box + (idx_rois*gt_box_count)] = 0;

            }
      } else {

        overlaps[idx_gt_box + (idx_rois*gt_box_count)] = 0;

      }
//        if (gt_box_count==1 && index==(count-1)){
////            overlaps[idx_gt_box + (idx_rois*gt_box_count)] = 0.5;
//            overlaps[idx_gt_box + (idx_rois*gt_box_count)-1] = query_boxes[k];
//            overlaps[idx_gt_box + (idx_rois*gt_box_count)-2] = query_boxes[k+1];
//            overlaps[idx_gt_box + (idx_rois*gt_box_count)-3] = query_boxes[k+2];
//            overlaps[idx_gt_box + (idx_rois*gt_box_count)-4] = query_boxes[k+3];
//            overlaps[idx_gt_box + (idx_rois*gt_box_count)-5] = boxes[n];
//            overlaps[idx_gt_box + (idx_rois*gt_box_count)-6] = boxes[n+1];
//            overlaps[idx_gt_box + (idx_rois*gt_box_count)-7] = boxes[n+2];
//            overlaps[idx_gt_box + (idx_rois*gt_box_count)-8] = boxes[n+3];
//            overlaps[idx_gt_box + (idx_rois*gt_box_count)-9] = iw;
//            overlaps[idx_gt_box + (idx_rois*gt_box_count)-10] = ih;
//            overlaps[idx_gt_box + (idx_rois*gt_box_count)-11] = query_box_area;
//            overlaps[idx_gt_box + (idx_rois*gt_box_count)-12] = box_area;
//        }
    }
  }
  __global__ void Compute_status(const float fg_thresh,
                                 const float bg_thresh_high,
                                 const float bg_thresh_low,
          const int count,
                                           const int gt_box_count,
          const float* overlaps,
                                 int* max_indices,
          uint8_t* status) {
    for (int index = blockIdx.x * blockDim.x + threadIdx.x;
         index < count;
         index += blockDim.x * gridDim.x) {
        int idx=index*gt_box_count;
        int max_idx=-1;
        float max_val=-1.0;
        uint8_t st=0;
        for (int i = 0; i <gt_box_count ; ++i) {
          if (overlaps[idx+i]>max_val){
            max_val=overlaps[idx+i];
            max_idx=i;
          }

        }
        if (max_val>=fg_thresh)
          st=1;
        else if(max_val>=bg_thresh_low && max_val<bg_thresh_high)
          st=2;
        status[index]=st;
        max_indices[index]=max_idx;


      }
  }
  template<typename Dtype>
  __global__ void Generate_target(
          const int count,
          const int gt_box_count,
          const int* keep_indices,
          const int actual_fg_count,
          const int* max_indices,
          const float* box_setting_info,
          const int num_class,
          const Dtype *difficult,
          const Dtype* gt_box,
          const Dtype* rois,
          Dtype* filterd_rois,
          Dtype* label,
          Dtype* bbox_weight,
          Dtype* bbox_target                ) {

    for (int index = blockIdx.x * blockDim.x + threadIdx.x;
         index < count;
         index += blockDim.x * gridDim.x) {
      int select_idx=keep_indices[index];
      int box_element_count_begin=index*4*num_class;
      //copy rois
      filterd_rois[index*5]=rois[select_idx*5];
      filterd_rois[index*5+1]=rois[select_idx*5+1];
      filterd_rois[index*5+2]=rois[select_idx*5+2];
      filterd_rois[index*5+3]=rois[select_idx*5+3];
      filterd_rois[index*5+4]=rois[select_idx*5+4];
      for (int i=0;i<num_class*4;i++) {
        bbox_weight[box_element_count_begin + i] = 0;
        bbox_target[box_element_count_begin + i] = 0;
      }

      if (index>=actual_fg_count){
        label[index]=0;
      }
      else{
        int _label=static_cast<int>(gt_box[max_indices[select_idx]*5+4]);
        // int _label=gt_box[max_indices[select_idx]*5+4];
        if (difficult[max_indices[select_idx]]<=0.5){
          // printf("if:%f,%f,%f,%f,%f,%f,%d\n",difficult[max_indices[select_idx]],gt_box[max_indices[select_idx]],gt_box[max_indices[select_idx]+1],gt_box[max_indices[select_idx]+2],gt_box[max_indices[select_idx]+3],gt_box[max_indices[select_idx]+4],_label);
          label[index]=_label;
        }
        else{
          // printf("else:%f,%f,%f,%f,%f,%f,%d\n",difficult[max_indices[select_idx]],gt_box[max_indices[select_idx]],gt_box[max_indices[select_idx]+1],gt_box[max_indices[select_idx]+2],gt_box[max_indices[select_idx]+3],gt_box[max_indices[select_idx]+4],_label);
          label[index]=-1;
        }

        for (int i =0;i<4;i++) {
          if (difficult[max_indices[select_idx]]<=0.5){
            bbox_weight[box_element_count_begin + _label * 4 + i] = box_setting_info[8 + i];
          }
          else{
            bbox_weight[box_element_count_begin + _label * 4 + i] = 0;
          }
        }
        auto ex_rois=rois+select_idx*5+1;
        auto gt_rois=gt_box+max_indices[select_idx]*5;
        Dtype ex_widths = ex_rois[2] - ex_rois[0] + 1.0;
        Dtype ex_heights = ex_rois[3] - ex_rois[1] + 1.0;
        Dtype ex_ctr_x = ex_rois[0] + 0.5 * (ex_widths - 1.0);
        Dtype ex_ctr_y = ex_rois[1] + 0.5 * (ex_heights - 1.0);

        Dtype gt_widths = gt_rois[2] - gt_rois[ 0] + 1.0;
        Dtype gt_heights = gt_rois[ 3] - gt_rois[ 1] + 1.0;
        Dtype gt_ctr_x = gt_rois[0] + 0.5 * (gt_widths - 1.0);
        Dtype gt_ctr_y = gt_rois[1] + 0.5 * (gt_heights - 1.0);

        bbox_target[box_element_count_begin + _label * 4 + 0] =
                ((gt_ctr_x - ex_ctr_x) / (ex_widths + 1e-14) - box_setting_info[0]) / box_setting_info[4];
        bbox_target[box_element_count_begin + _label * 4 + 1] =
                ((gt_ctr_y - ex_ctr_y) / (ex_heights + 1e-14) - box_setting_info[1]) / box_setting_info[5];
        bbox_target[box_element_count_begin + _label * 4 + 2] =
                (log(gt_widths / ex_widths) - box_setting_info[2]) / box_setting_info[6];
        bbox_target[box_element_count_begin + _label * 4 + 3] =
                (log(gt_heights / ex_heights) - box_setting_info[3]) / box_setting_info[7];

      }
    }
  }

}  // namespace cuda
}  // namespace mshadow

namespace mxnet {
namespace op {

template<typename xpu>
class Proposal_targetGPUOp : public Operator{
 public:
  explicit Proposal_targetGPUOp(Proposal_targetParam param) {
    this->param_ = param;
  }

  virtual void Forward(const OpContext &ctx,
                       const std::vector<TBlob> &in_data,
                       const std::vector<OpReqType> &req,
                       const std::vector<TBlob> &out_data,
                       const std::vector<TBlob> &aux_states) {
    using namespace mshadow;
    using namespace mshadow::expr;
    using namespace mshadow::cuda;
    CHECK_EQ(in_data.size(), 4);
    CHECK_EQ(out_data.size(), 4);
    CHECK_GT(req.size(), 1);
    CHECK_EQ(req[proposal_target::kfilted_rois], kWriteTo);
    Stream<xpu> *s = ctx.get_stream<xpu>();
    Tensor<xpu, 2> rois = in_data[proposal_target::krois].get<xpu, 2, real_t>(s);
    Tensor<xpu, 2> gt_box = in_data[proposal_target::kgt_box].get<xpu, 2, real_t>(s);
    Tensor<xpu, 2> gt_box_info = in_data[proposal_target::kgt_box_info].get<xpu, 2, real_t>(s);
    Tensor<xpu, 2> difficult = in_data[proposal_target::kdifficult].get<xpu, 2, real_t>(s);
    Tensor<xpu, 2> filted_rois = out_data[proposal_target::kfilted_rois].get<xpu, 2, real_t>(s);
    Tensor<xpu, 2> box_target = out_data[proposal_target::kbox_target].get<xpu, 2, real_t>(s);
    Tensor<xpu, 2> box_weight = out_data[proposal_target::kbox_weight].get<xpu, 2, real_t>(s);
    Tensor<xpu, 1> label_out = out_data[proposal_target::klabel].get<xpu, 1,real_t>(s);
    std::vector<float> cpu_gt_box_info(1);
    FRCNN_CUDA_CHECK(cudaMemcpy(&cpu_gt_box_info[0], gt_box_info.dptr_,
                                sizeof(float) * cpu_gt_box_info.size(),
                                cudaMemcpyDeviceToHost));
    int num_gt_box=int(cpu_gt_box_info[0]);
    int rois_count=rois.size(0);
    int added_rois_count=num_gt_box+rois_count;
    std::vector<int> selected_indexs;
    int fg_rois_per_this_image=0;
    int count_fg = 0;
    int count_bg = 0;
    dim3 dimGrid((num_gt_box + kMaxThreadsPerBlock - 1) / kMaxThreadsPerBlock);
    dim3 dimBlock(kMaxThreadsPerBlock);
    if (num_gt_box!=0) {
      dim3 dimGrid((num_gt_box + kMaxThreadsPerBlock - 1) / kMaxThreadsPerBlock);
      dim3 dimBlock(kMaxThreadsPerBlock);
      float *added_rois_ptr = NULL;
      FRCNN_CUDA_CHECK(cudaMalloc(&added_rois_ptr, sizeof(float) * added_rois_count * 5));
      Tensor<xpu, 2,float> added_rois(added_rois_ptr, Shape2(added_rois_count, 5));
      FRCNN_CUDA_CHECK(cudaMemcpy(added_rois.dptr_,
                                  rois.dptr_, sizeof(float) * rois.shape_.Size(),
                                  cudaMemcpyDeviceToDevice));
      Add_gt_to_rois << < dimGrid, dimBlock >> > (num_gt_box, gt_box.dptr_, added_rois.dptr_ + rois_count * 5);
      FRCNN_CUDA_CHECK(cudaPeekAtLastError());


      float *overlaps_ptr = NULL;
      FRCNN_CUDA_CHECK(cudaMalloc(&overlaps_ptr, sizeof(float) * added_rois_count * num_gt_box));
      Tensor<xpu, 2,float> overlaps(overlaps_ptr, Shape2(added_rois_count, num_gt_box));
      dimGrid.x = (added_rois_count * num_gt_box + kMaxThreadsPerBlock - 1) / kMaxThreadsPerBlock;
      Bbox_overlaps <<< dimGrid, dimBlock >>> (added_rois_count * num_gt_box,
              num_gt_box,
              gt_box.dptr_,
              added_rois.dptr_,
              overlaps.dptr_);
      FRCNN_CUDA_CHECK(cudaPeekAtLastError());
//        //        check
//        std::cout<<"check_begin----------------------"<<std::endl;
//        int check_count=1;
//        std::vector<float > cpu_gt(check_count);
//        FRCNN_CUDA_CHECK(cudaMemcpy(&cpu_gt[0],
//                                    overlaps.dptr_+rois_count-check_count+1, sizeof(float) *check_count,
//                                    cudaMemcpyDeviceToHost));
////        std::vector<float > cpu_check(check_count);
////        FRCNN_CUDA_CHECK(cudaMemcpy(&cpu_check[0],
////                                    added_rois.dptr_+rois_count*5, sizeof(float) * check_count,
////                                    cudaMemcpyDeviceToHost));
//        std::cout<<rois_count<<""<<num_gt_box<<" "<<overlaps.shape_.Size()<<std::endl;
//        for (int k = check_count-1; k >= 0; --k) {
////            std::cout<<cpu_gt[k]<<":"<<cpu_check[k]<<std::endl;
//            std::cout<<cpu_gt[k]<<std::endl;
//        }
//        std::cout<<"check_end----------------"<<std::endl;
////        check_end
      int *max_indices_ptr = NULL;
      FRCNN_CUDA_CHECK(cudaMalloc(&max_indices_ptr, sizeof(int) * added_rois_count));
      Tensor<xpu, 1,int> max_indices(max_indices_ptr, Shape1(added_rois_count));
      uint8_t *status_ptr = NULL;
      FRCNN_CUDA_CHECK(cudaMalloc(&status_ptr, sizeof(uint8_t) * added_rois_count));
      Tensor<xpu, 1,uint8_t> status(status_ptr, Shape1(added_rois_count));
      dimGrid.x = (added_rois_count + kMaxThreadsPerBlock - 1) / kMaxThreadsPerBlock;
      Compute_status <<< dimGrid, dimBlock >>> (param_.fg_thresh,
              param_.bg_thresh_high,
              param_.bg_thresh_low,
              added_rois_count,
              num_gt_box,
              overlaps.dptr_,
              max_indices.dptr_,
              status.dptr_);
      FRCNN_CUDA_CHECK(cudaPeekAtLastError());

      std::vector<uint8_t> cpu_status(added_rois_count);
      FRCNN_CUDA_CHECK(cudaMemcpy(&cpu_status[0], status.dptr_,
                                  sizeof(uint8_t) * added_rois_count,
                                  cudaMemcpyDeviceToHost));
      std::vector<int> fg_indexes(added_rois_count);
      std::vector<int> bg_indexes(added_rois_count);

      for (int i = 0; i < added_rois_count; ++i) {
        if (cpu_status[i] == 1) {
          fg_indexes[count_fg] = i;
          count_fg++;
        } else if (cpu_status[i] == 2) {
          bg_indexes[count_bg] = i;
          count_bg++;
        }
      }

      // std::vector<real_t> cpu_gt_box(5);
      // FRCNN_CUDA_CHECK(cudaMemcpy(&cpu_gt_box[0], gt_box.dptr_,
      //                             sizeof(real_t) * 5,
      //                             cudaMemcpyDeviceToHost));
      // printf("gt_box:%f,%f,%f,%f,%f",cpu_gt_box[0],cpu_gt_box[1],cpu_gt_box[2],cpu_gt_box[3],cpu_gt_box[4]);

      int fg_rois_per_image = int(param_.batch_rois * param_.fg_fraction);
      fg_rois_per_this_image = min(fg_rois_per_image, count_fg);

      if (count_fg > fg_rois_per_this_image) {
        std::vector<int> need_shullf_indexes(count_fg);
        for (int i = 0; i < count_fg; ++i) {
          need_shullf_indexes[i] = i;
        }
        std::random_shuffle(need_shullf_indexes.begin(), need_shullf_indexes.end());
        for (int j = 0; j < fg_rois_per_this_image; ++j) {
          selected_indexs.push_back(fg_indexes[need_shullf_indexes[j]]);
        }
      } else {
        selected_indexs.insert(selected_indexs.end(), fg_indexes.begin(), fg_indexes.begin() + count_fg);
      }
      int bg_rois_per_this_image = param_.batch_rois - fg_rois_per_this_image;
      bg_rois_per_this_image = min(bg_rois_per_this_image, count_bg);
      if (count_bg > bg_rois_per_this_image) {
        std::vector<int > need_shullf_indexes(count_bg);
        for (int i = 0; i < count_bg; ++i) {
          need_shullf_indexes[i]=i;
        }
        std::random_shuffle(need_shullf_indexes.begin(),need_shullf_indexes.end());
        for (int j = 0; j < bg_rois_per_this_image; ++j) {
          selected_indexs.push_back(bg_indexes[need_shullf_indexes[j]]);
        }
      }
      else{
        selected_indexs.insert(selected_indexs.end(),bg_indexes.begin(),bg_indexes.begin()+count_bg);
      }
      if (selected_indexs.size()<param_.batch_rois){
        int count_gap=param_.batch_rois-selected_indexs.size();
        int multi_bg_count=count_gap/count_bg;
        int remain_bg_count=count_gap%count_bg;
        while(multi_bg_count!=0){
          selected_indexs.insert(selected_indexs.end(),bg_indexes.begin(),bg_indexes.end());
        }
        selected_indexs.insert(selected_indexs.end(),bg_indexes.begin(),bg_indexes.begin()+remain_bg_count);
      }
      std::vector<float> cpu_box_setting_info;
      cpu_box_setting_info.insert(cpu_box_setting_info.end(),param_.bbox_means.info.begin(),param_.bbox_means.info.end());
      cpu_box_setting_info.insert(cpu_box_setting_info.end(),param_.bbox_std.info.begin(),param_.bbox_std.info.end());
      cpu_box_setting_info.insert(cpu_box_setting_info.end(),param_.bbox_weight.info.begin(),param_.bbox_weight.info.end());
      float *box_setting_info_ptr = NULL;
      FRCNN_CUDA_CHECK(cudaMalloc(&box_setting_info_ptr, sizeof(float) * 12));
      Tensor<xpu, 1,float> box_setting_info(box_setting_info_ptr, Shape1(12));
      FRCNN_CUDA_CHECK(cudaMemcpy(box_setting_info.dptr_,
                                  &cpu_box_setting_info[0], sizeof(float) *12,
                                  cudaMemcpyHostToDevice));

      int *keep_indexes_ptr = NULL;
      FRCNN_CUDA_CHECK(cudaMalloc(&keep_indexes_ptr, sizeof(int) * selected_indexs.size()));
      Tensor<xpu, 1,int> keep_indexes(keep_indexes_ptr, Shape1(selected_indexs.size()));
      FRCNN_CUDA_CHECK(cudaMemcpy(keep_indexes.dptr_,
                                  &selected_indexs[0], sizeof(int) *selected_indexs.size(),
                                  cudaMemcpyHostToDevice));
      dimGrid.x = (param_.batch_rois + kMaxThreadsPerBlock - 1) / kMaxThreadsPerBlock;
      Generate_target << < dimGrid, dimBlock >> > (
              param_.batch_rois,
                      num_gt_box,
                      keep_indexes.dptr_,
                      fg_rois_per_this_image,
                      max_indices.dptr_,
                      box_setting_info.dptr_,
                      param_.num_class,
                      difficult.dptr_,
                      gt_box.dptr_,
                      added_rois.dptr_,
                      filted_rois.dptr_,
                      label_out.dptr_,
                      box_weight.dptr_,
                      box_target.dptr_
      );
        //std::cout<<param_.batch_rois<<" "<<param_.fg_fraction<<" "<<num_gt_box<<" "<<count_fg<<" "<<count_bg<<" "<<rois_count<<" "<<added_rois_count<<std::endl;
      FRCNN_CUDA_CHECK(cudaPeekAtLastError());
      FRCNN_CUDA_CHECK(cudaFree(added_rois_ptr));
      FRCNN_CUDA_CHECK(cudaFree(box_setting_info_ptr));
      FRCNN_CUDA_CHECK(cudaFree(max_indices_ptr));
      FRCNN_CUDA_CHECK(cudaFree(status_ptr));
      FRCNN_CUDA_CHECK(cudaFree(overlaps_ptr));
      FRCNN_CUDA_CHECK(cudaFree(keep_indexes_ptr));
    }
    else{
      fg_rois_per_this_image=0;
      count_bg=rois_count;
      std::vector<int> bg_indexes(rois_count);
      for (int k = 0; k <rois_count ; ++k) {
        bg_indexes[k]=k;
      }
      int bg_rois_per_this_image = param_.batch_rois - fg_rois_per_this_image;
      bg_rois_per_this_image = min(bg_rois_per_this_image, count_bg);
      if (count_bg > bg_rois_per_this_image) {
        std::vector<int > need_shullf_indexes(count_bg);
        for (int i = 0; i < count_bg; ++i) {
          need_shullf_indexes[i]=i;
        }
        std::random_shuffle(need_shullf_indexes.begin(),need_shullf_indexes.end());
        for (int j = 0; j < bg_rois_per_this_image; ++j) {
          selected_indexs.push_back(bg_indexes[need_shullf_indexes[j]]);
        }
      }
      else{
        selected_indexs.insert(selected_indexs.end(),bg_indexes.begin(),bg_indexes.begin()+count_bg);
      }
      if (selected_indexs.size()<param_.batch_rois){
        int count_gap=param_.batch_rois-selected_indexs.size();
        int multi_bg_count=count_gap/count_bg;
        int remain_bg_count=count_gap%count_bg;
        while(multi_bg_count!=0){
          selected_indexs.insert(selected_indexs.end(),bg_indexes.begin(),bg_indexes.end());
          multi_bg_count--;
        }
        selected_indexs.insert(selected_indexs.end(),bg_indexes.begin(),bg_indexes.begin()+remain_bg_count);
      }
      std::vector<float> cpu_box_setting_info;
      cpu_box_setting_info.insert(cpu_box_setting_info.end(),param_.bbox_means.info.begin(),param_.bbox_means.info.end());
      cpu_box_setting_info.insert(cpu_box_setting_info.end(),param_.bbox_std.info.begin(),param_.bbox_std.info.end());
      cpu_box_setting_info.insert(cpu_box_setting_info.end(),param_.bbox_weight.info.begin(),param_.bbox_weight.info.end());
      float *box_setting_info_ptr = NULL;
      FRCNN_CUDA_CHECK(cudaMalloc(&box_setting_info_ptr, sizeof(float) * 12));
      Tensor<xpu, 1,float> box_setting_info(box_setting_info_ptr, Shape1(12));
      FRCNN_CUDA_CHECK(cudaMemcpy(box_setting_info.dptr_,
                                  &cpu_box_setting_info[0], sizeof(float) *12,
                                  cudaMemcpyHostToDevice));

      int *keep_indexes_ptr = NULL;
      FRCNN_CUDA_CHECK(cudaMalloc(&keep_indexes_ptr, sizeof(int) * selected_indexs.size()));
      Tensor<xpu, 1,int> keep_indexes(keep_indexes_ptr, Shape1(selected_indexs.size()));
      FRCNN_CUDA_CHECK(cudaMemcpy(keep_indexes.dptr_,
                                  &selected_indexs[0], sizeof(int) *selected_indexs.size(),
                                  cudaMemcpyHostToDevice));
      dimGrid.x = (param_.batch_rois + kMaxThreadsPerBlock - 1) / kMaxThreadsPerBlock;
      Generate_target << < dimGrid, dimBlock >> > (
              param_.batch_rois,
                      num_gt_box,
                      keep_indexes.dptr_,
                      fg_rois_per_this_image,
                      keep_indexes.dptr_,
                      box_setting_info.dptr_,
                      param_.num_class,
                      difficult.dptr_,
                      gt_box.dptr_,
                      rois.dptr_,
                      filted_rois.dptr_,
                      label_out.dptr_,
                      box_weight.dptr_,
                      box_target.dptr_
      );
      FRCNN_CUDA_CHECK(cudaPeekAtLastError());
      FRCNN_CUDA_CHECK(cudaFree(box_setting_info_ptr));
      FRCNN_CUDA_CHECK(cudaFree(keep_indexes_ptr));

    }

  }

  virtual void Backward(const OpContext &ctx,
                        const std::vector<TBlob> &out_grad,
                        const std::vector<TBlob> &in_data,
                        const std::vector<TBlob> &out_data,
                        const std::vector<OpReqType> &req,
                        const std::vector<TBlob> &in_grad,
                        const std::vector<TBlob> &aux_states) {
    using namespace mshadow;
    using namespace mshadow::expr;
    CHECK_EQ(in_grad.size(), 3);

    
    Stream<xpu> *s = ctx.get_stream<xpu>();
    Tensor<xpu, 2> rois = in_grad[proposal_target::krois].get<xpu, 2, real_t>(s);
    Tensor<xpu, 2> gt_box = in_grad[proposal_target::kgt_box].get<xpu, 2, real_t>(s);
    Tensor<xpu, 2> box_info = in_grad[proposal_target::kgt_box_info].get<xpu, 2, real_t>(s);

    // can not assume the grad would be zero
    Assign(rois, req[proposal_target::krois], 0);
    Assign(gt_box, req[proposal_target::kgt_box], 0);
    Assign(box_info, req[proposal_target::kgt_box_info], 0);
  }

 private:
  Proposal_targetParam param_;
};  // class ProposalGPUOp

template<>
Operator* CreateOp<gpu>(Proposal_targetParam param) {
  return new Proposal_targetGPUOp<gpu>(param);
}
}  // namespace op
}  // namespace mxnet
