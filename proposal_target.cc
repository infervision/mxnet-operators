

/*!
 * \file proposal_target.cc
 * \brief
*/

#include "proposal_target-inl.h"

namespace mxnet {
    namespace op {

        template<typename xpu>
        class Proposal_targetOp : public Operator{
        public:
            explicit Proposal_targetOp(Proposal_targetParam param) {
              this->param_ = param;
            }

            virtual void Forward(const OpContext &ctx,
                                 const std::vector<TBlob> &in_data,
                                 const std::vector<OpReqType> &req,
                                 const std::vector<TBlob> &out_data,
                                 const std::vector<TBlob> &aux_states) {
              return;

            }

            virtual void Backward(const OpContext &ctx,
                                  const std::vector<TBlob> &out_grad,
                                  const std::vector<TBlob> &in_data,
                                  const std::vector<TBlob> &out_data,
                                  const std::vector<OpReqType> &req,
                                  const std::vector<TBlob> &in_grad,
                                  const std::vector<TBlob> &aux_states) {
              return;
            }

        private:
            Proposal_targetParam param_;
        };  // class Proposal_targetOp

        template<>
        Operator *CreateOp<cpu>(Proposal_targetParam param) {
          std::cout<<"Proposal_target_cpu"<<std::endl;
          return new Proposal_targetOp<cpu>(param);
        }

        Operator* Proposal_targetProp::CreateOperator(Context ctx) const {
//  std::cout<<"Proposal_target_Prop"<<std::endl;
          DO_BIND_DISPATCH(CreateOp, param_);
        }

        DMLC_REGISTER_PARAMETER(Proposal_targetParam);

        MXNET_REGISTER_OP_PROPERTY(_contrib_Proposal_target, Proposal_targetProp)
        .describe("Generate region proposals via RPN")
        .add_argument("rois", "NDArray-or-Symbol", "Score of how likely proposal is object.")
        .add_argument("gt_box", "NDArray-or-Symbol", "Score of how likely proposal is object.")
        .add_argument("gt_box_info", "NDArray-or-Symbol", "Score of how likely proposal is object.")
        .add_argument("difficult", "NDArray-or-Symbol", "Score of how likely proposal is object.")
        .add_arguments(Proposal_targetParam::__FIELDS__());

    }  // namespace op
}  // namespace mxnet
