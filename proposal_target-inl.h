

/*!
 * \file proposal_target-inl.h
 * \brief Proposal_target Operator
*/
#ifndef MXNET_OPERATOR_CONTRIB_PROPOSAL_TARGET_INL_H_
#define MXNET_OPERATOR_CONTRIB_PROPOSAL_TARGET_INL_H_

#include <dmlc/logging.h>
#include <dmlc/parameter.h>
#include <mxnet/operator.h>
#include <map>
#include <vector>
#include <string>
#include <utility>
#include <ctime>
#include <cstring>
#include <iostream>
#include "./operator_common.h"
#include "./mshadow_op.h"

// extend NumericalParam
namespace mxnet {
namespace op {

/*!
* \brief structure for numerical tuple input
* \tparam VType data type of param
*/
template<typename VType>
struct NumericalParam {
  NumericalParam() {}
  explicit NumericalParam(VType *begin, VType *end) {
    int32_t size = static_cast<int32_t>(end - begin);
    info.resize(size);
    for (int i = 0; i < size; ++i) {
      info[i] = *(begin + i);
    }
  }
  inline size_t ndim() const {

    return info.size();
  }
  std::vector<VType> info;
};

template<typename VType>
inline std::istream &operator>>(std::istream &is, NumericalParam<VType> &param) {
  while (true) {
    char ch = is.get();
    if (ch == '(') break;
    if (!isspace(ch)) {
      is.setstate(std::ios::failbit);
      return is;
    }
  }
  VType idx;
  std::vector<VType> tmp;
  // deal with empty case
  size_t pos = is.tellg();
  char ch = is.get();
  if (ch == ')') {
    param.info = tmp;
    return is;
  }
  is.seekg(pos);
  // finish deal
  while (is >> idx) {
    tmp.push_back(idx);
    char ch;
    do {
      ch = is.get();
    } while (isspace(ch));
    if (ch == ',') {
      while (true) {
        ch = is.peek();
        if (isspace(ch)) {
          is.get(); continue;
        }
        if (ch == ')') {
          is.get(); break;
        }
        break;
      }
      if (ch == ')') break;
    } else if (ch == ')') {
      break;
    } else {
      is.setstate(std::ios::failbit);
      return is;
    }
  }
  param.info = tmp;
  return is;
}

template<typename VType>
inline std::ostream &operator<<(std::ostream &os, const NumericalParam<VType> &param) {
  os << '(';
  for (index_t i = 0; i < param.info.size(); ++i) {
    if (i != 0) os << ',';
    os << param.info[i];
  }
  // python style tuple
  if (param.info.size() == 1) os << ',';
  os << ')';
  return os;
}

}  // namespace op
}  // namespace mxnet

namespace mxnet {
namespace op {

namespace proposal_target {
enum Proposal_targetOpInputs {krois, kgt_box, kgt_box_info,kdifficult};
enum Proposal_targetOpOutputs {kfilted_rois, klabel,kbox_target,kbox_weight};
enum Proposal_targetForwardResource {kTempResource};
}  // proposal

struct Proposal_targetParam : public dmlc::Parameter<Proposal_targetParam> {
  float fg_thresh;
  float bg_thresh_high;
  float bg_thresh_low;
    float fg_fraction;
    int batch_rois;
  NumericalParam<float> bbox_std;
  NumericalParam<float> bbox_means;
  NumericalParam<float> bbox_weight;
  int num_class;
  DMLC_DECLARE_PARAMETER(Proposal_targetParam) {
    float tmp[] = {0, 0, 0, 0};
    DMLC_DECLARE_FIELD(fg_thresh).set_default(0.5)
    .describe("Number of top scoring boxes to keep after applying NMS to RPN proposals");
    DMLC_DECLARE_FIELD(bg_thresh_high).set_default(0.1)
    .describe("Overlap threshold used for non-maximum"
              "suppresion(suppress boxes with IoU >= this threshold");
    DMLC_DECLARE_FIELD(bg_thresh_low).set_default(0.0)
    .describe("NMS value, below which to suppress.");
    DMLC_DECLARE_FIELD(fg_fraction).set_default(0.33333)
    .describe("Minimum height or width in proposal");
    DMLC_DECLARE_FIELD(batch_rois).set_default(32)
   .describe("Minimum height or width in proposal");
    tmp[0] = 0.1f; tmp[1] = 0.1f; tmp[2] = 0.2f; tmp[3] = 0.2f;
    DMLC_DECLARE_FIELD(bbox_std).set_default(NumericalParam<float>(tmp, tmp + 4))
    .describe("Used to generate anchor windows by enumerating scales");
    tmp[0] = 0.0f; tmp[1] = 0.0f; tmp[2] = 0.0f;tmp[3] = 0.0f;
    DMLC_DECLARE_FIELD(bbox_means).set_default(NumericalParam<float>(tmp, tmp + 4))
    .describe("Used to generate anchor windows by enumerating ratios");
      tmp[0] = 1.0f; tmp[1] = 1.0f; tmp[2] = 1.0f;tmp[3] = 1.0f;
      DMLC_DECLARE_FIELD(bbox_weight).set_default(NumericalParam<float>(tmp, tmp + 4))
      .describe("Used to generate anchor windows by enumerating ratios");
      DMLC_DECLARE_FIELD(num_class).set_default(2)
      .describe("Minimum height or width in proposal");
  }
};

template<typename xpu>
Operator *CreateOp(Proposal_targetParam param);

#if DMLC_USE_CXX11
class Proposal_targetProp : public OperatorProperty {
 public:
  void Init(const std::vector<std::pair<std::string, std::string> >& kwargs) override {
    param_.Init(kwargs);
  }

  std::map<std::string, std::string> GetParams() const override {
    return param_.__DICT__();
  }

  bool InferShape(std::vector<TShape> *in_shape,
                  std::vector<TShape> *out_shape,
                  std::vector<TShape> *aux_shape) const override {
    using namespace mshadow;
    CHECK_EQ(in_shape->size(), 4) << "Input:[rois, gt_box, box_info]";

    Shape<2> box_info_shape;
    box_info_shape = Shape2(1, 1);
    SHAPE_ASSIGN_CHECK(*in_shape, proposal_target::kgt_box_info, box_info_shape);
    out_shape->clear();
    // filted_rois
    out_shape->push_back(Shape2(param_.batch_rois,5));
    // label
    out_shape->push_back(Shape1(param_.batch_rois));
    // box_target
    out_shape->push_back(Shape2(param_.batch_rois, 4*param_.num_class));
    // box_weight
    out_shape->push_back(Shape2(param_.batch_rois, 4*param_.num_class));
    return true;
  }

  OperatorProperty* Copy() const override {
    auto ptr = new Proposal_targetProp();
    ptr->param_ = param_;
    return ptr;
  }

  std::string TypeString() const override {
    return "_contrib_Proposal_target";
  }

  std::vector<ResourceRequest> ForwardResource(
      const std::vector<TShape> &in_shape) const override {
    return {ResourceRequest::kTempSpace};
  }

  std::vector<int> DeclareBackwardDependency(
    const std::vector<int> &out_grad,
    const std::vector<int> &in_data,
    const std::vector<int> &out_data) const override {
    return {};
  }

  int NumVisibleOutputs() const override {
    return 4;
  }

  int NumOutputs() const override {
    return 4;
  }

  std::vector<std::string> ListArguments() const override {
    return {"rois", "gt_box", "gt_box_info","difficult"};
  }

  std::vector<std::string> ListOutputs() const override {
    return {"filted_rois", "label","box_target","box_weight"};
  }

  Operator* CreateOperator(Context ctx) const override;

 private:
  Proposal_targetParam param_;
};  // class ProposalProp

#endif  // DMLC_USE_CXX11
}  // namespace op
}  // namespace mxnet


#endif  //  MXNET_OPERATOR_CONTRIB_PROPOSAL_TARGET_INL_H_
